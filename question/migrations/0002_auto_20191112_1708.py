# Generated by Django 2.2.7 on 2019-11-12 17:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('question', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='like',
            unique_together={('content_type', 'object_id', 'author')},
        ),
    ]
