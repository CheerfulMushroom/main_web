# Web project "Ask Pumpkin"
##### Made by Alexandr Lebedev

This website is halloween theme site where spooky skeletons  or ghosts can ask all kinds of questions.

But before you start make sure:
- you have necessary modules installed:

`pip install -r requirements.txt`

- you made migrations

`./manage.py makemigrations`

`./manage.py migrate`

- you filled DB with data

`./manage.py refill_db`

- Great! Now everything is ready. Just run server

`./manage.py runserver`

Have a spooky halloween :)

░░░░░░░░░░░░▄▐  
░░░░░░▄▄▄░░▄██▄  
░░░░░▐▀█▀▌░░░░▀█▄  
░░░░░▐█▄█▌░░░░░░▀█▄  
░░░░░░▀▄▀░░░▄▄▄▄▄▀▀  
░░░░▄▄▄██▀▀▀▀  
░░░█▀▄▄▄█░▀▀  
░░░▌░▄▄▄▐▌▀▀▀  
▄░▐░░░▄▄░█░▀▀ U HAVE BEEN SPOOKED BY THE  
▀█▌░░░▄░▀█▀░▀  
░░░░░░░▄▄▐▌▄▄  
░░░░░░░▀███▀█░▄  
░░░░░░▐▌▀▄▀▄▀▐▄SPOOKY SKILENTON  
░░░░░░▐▀░░░░░░▐▌  
░░░░░░█░░░░░░░░█  
░░░░░▐▌░░░░░░░░░█  
░░░░░█░░░░░░░░░░▐▌UPDOOT THIS TO 7 PPL OR SKELINTONS WILL EAT YOU  

